﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOverManagment : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && gameObject.activeSelf)
        {
            Time.timeScale = 1f;
            gameObject.SetActive(false);
            AutoFade.LoadLevel("BattleScene", 1, 1, Color.black);
        }
        if(Input.GetKeyDown(KeyCode.Escape) && gameObject.activeSelf)
        {
            Time.timeScale = 1f;
            gameObject.SetActive(false);
            AutoFade.LoadLevel("MainMenu", 1, 1, Color.black);
        }
    }
}
