﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Audio;
public class GeneratorSlova : MonoBehaviour
{
    public AudioClip sound_slovo1;
    public AudioClip sound_slovo2;
    public AudioClip sound_slovo3;
    public AudioClip sound_slovo4;
    public AudioClip sound_slovo5;
    public AudioClip sound_slovo6;
    public AudioClip sound_nova_slova;
    public AudioClip sound_makni_slovo;

    public Button button;
    public GridLayoutGroup layoutGeneratedLetters;
    public GridLayoutGroup layoutPressedLetters;
    public Button btn_attack;

    public Game_Managment gm;

    public TextMeshProUGUI lbl_money;

    public int brojac = 0;

    public int bonusAttack = 0;
    char[] samoglasnici = { 'A', 'E', 'I', 'O', 'U' };
    char[] ostala_slova = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'V', 'Z' };
    char[] rijetka_slova = { 'Č', 'Ć', 'Đ', 'Š', 'Ž' };
    //List<char> slova = new List<char>();

    private void Start()
    {
        brojac = 0;
        gm = GameObject.FindObjectOfType(typeof(Game_Managment)) as Game_Managment;
    }
    public void GenerirajSlova()
    {
        int samoglasnici_counter = 0;
        int rijetka_slova_counter = 0;
        Game_Managment.slova.Clear();

        for (int i = 0; i < 25; i++)
        {
            Button newButton = Instantiate(button) as Button;
            if (UnityEngine.Random.Range(0, 3) == 0 && samoglasnici_counter < 6)
            {
                samoglasnici_counter++;
                newButton.GetComponentInChildren<Text>().text = GenerirajSlovo(0);
            }
            else if (UnityEngine.Random.Range(0, 3) == 1 && rijetka_slova_counter < 2)
            {
                rijetka_slova_counter++;
                newButton.GetComponentInChildren<Text>().text = GenerirajSlovo(1);
            }
            else
            {
                newButton.GetComponentInChildren<Text>().text = GenerirajSlovo(2);
            }
            if (UnityEngine.Random.Range(0, 12) == 1)
            {
                newButton.GetComponentInChildren<Text>().color = Color.green;
                newButton.name = "GreenBTN_" + newButton.GetComponentInChildren<Text>().text + "_" + i;
            }
            else if (UnityEngine.Random.Range(0, 50) == 1)
            {
                newButton.GetComponentInChildren<Text>().color = Color.red;
                newButton.name = "RedBTN_" + newButton.GetComponentInChildren<Text>().text + "_" + i;
            }
            else
            {
                newButton.name = "BTN_" + newButton.GetComponentInChildren<Text>().text + "_" + i;
            }
            Game_Managment.slova.Add(newButton.GetComponentInChildren<Text>().text[0]);
            newButton.GetComponent<Button>().onClick.AddListener(delegate { button_Click(newButton); });
            newButton.transform.SetParent(layoutGeneratedLetters.transform, false);
        }
        
    }
    public void button_Click(Button btn)
    {
        Button newButton = Instantiate(button) as Button;
        newButton.GetComponentInChildren<Text>().text = btn.GetComponentInChildren<Text>().text;
        newButton.name = "Copy_" + btn.name;

        if (btn.name.Contains("Green"))
        {
            newButton.GetComponentInChildren<Text>().color = Color.green;
            bonusAttack += 2;
        }
        else if (btn.name.Contains("Red"))
        {
            newButton.GetComponentInChildren<Text>().color = Color.red;
            bonusAttack += 5;
        }
        else
        {
            newButton.GetComponentInChildren<Text>().color = Color.black;
            bonusAttack += 1;
        }

        newButton.GetComponent<Button>().onClick.AddListener(delegate { Remove_letter_Click(newButton, btn); });
        newButton.transform.SetParent(layoutPressedLetters.transform, false);
        btn.interactable = false;
        Word();
        brojac++;

        if(brojac > 0 && brojac < 4)
            this.GetComponent<AudioSource>().clip = sound_slovo1;
        else if(brojac == 4)
            this.GetComponent<AudioSource>().clip = sound_slovo2;
        else if (brojac == 5)
            this.GetComponent<AudioSource>().clip = sound_slovo3;
        else if (brojac == 6)
            this.GetComponent<AudioSource>().clip = sound_slovo4;
        else if (brojac == 7)
            this.GetComponent<AudioSource>().clip = sound_slovo5;
        else if (brojac == 8)
            this.GetComponent<AudioSource>().clip = sound_slovo6;

        this.GetComponent<AudioSource>().Play();
        gm.refreshStats();
        //Debug.Log(btn.name.Split('_')[2]);
    }
    void Word()
    {
        Game_Managment.word = "";
        foreach (Button btn in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn.name.Contains("Copy"))
            {
                Game_Managment.word += btn.name.Split('_')[2];
            }
        }
        Check_if_word_exists(Reverse(Game_Managment.word));
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
    void Check_if_word_exists(string word)
    {
        bool postoji = false;
        //Debug.Log("Entered word: " + word);
        foreach (string imenica in Game_Managment.imenice)
        {
            if (imenica.ToUpper() == word)
            {
                // btn_attack.interactable = true;
                //Debug.Log("Entered word: " + word + " found!");
                try
                {
                    foreach (string i in Game_Managment.spremljene_rijeci)
                    {
                        if (i == word)
                        {
                            postoji = true;
                            break;
                        }
                        else { postoji = false; }
                    }

                    if (!postoji)
                    {
                        btn_attack.interactable = true;
                    }
                    else { btn_attack.interactable = false; }

                    break;
                }
                catch { }

            }
            else
            {
                btn_attack.interactable = false;
            }
        }
    }
    protected void Remove_letter_Click(Button btn, Button previous_btn)
    {
        //Debug.Log("Letter: "+ btn.name.Split('_')[2]+ " was removed." );
        this.GetComponent<AudioSource>().clip = sound_makni_slovo;
        this.GetComponent<AudioSource>().Play();

        if (btn.name.Contains("Green"))
        {
            bonusAttack -= 2;
        }
        else if (btn.name.Contains("Red"))
        {
            bonusAttack -= 5;
        }
        else
        {
            bonusAttack -= 1;
        }

        previous_btn.interactable = true;
        brojac--;
        Word_On_Delete(btn);
        Destroy(btn);
        Destroy(btn.gameObject);
        gm.refreshStats();
    }
    void Word_On_Delete(Button btn)
    {
        Game_Managment.word = "";
        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("Copy") && btn1.name.Split('_')[3] != btn.name.Split('_')[3])
            {
                Game_Managment.word += btn1.name.Split('_')[2];
            }
        }
        Check_if_word_exists(Reverse(Game_Managment.word));
    }

    public string GenerirajSlovo(int num)
    {
        string slovo = "";
        int c = 0;
        if (num == 0)
        {
            slovo = samoglasnici[UnityEngine.Random.Range(0, 5)].ToString();
            while (true)
            {
                if (Game_Managment.slova.Count > 3)
                {
                    foreach (char s in Game_Managment.slova)
                    {
                        if (s.ToString() == slovo)
                            c++;
                    }
                }
                if (c >= 3)
                {
                    c = 0;
                    slovo = samoglasnici[UnityEngine.Random.Range(0, 5)].ToString();
                }
                else { break; }
            }
        }

        if (num == 1)
        {
            slovo = rijetka_slova[UnityEngine.Random.Range(0, 5)].ToString();
            while (true)
            {
                if (Game_Managment.slova.Count > 3)
                {
                    foreach (char s in Game_Managment.slova)
                    {
                        if (s.ToString() == slovo)
                            c++;
                    }
                }
                if (c >= 3)
                {
                    c = 0;
                    slovo = rijetka_slova[UnityEngine.Random.Range(0, 5)].ToString();
                }
                else { break; }
            }
        }

        if (num == 2)
        {
            slovo = ostala_slova[UnityEngine.Random.Range(0, 17)].ToString();
            while (true)
            {
                if (Game_Managment.slova.Count > 3)
                {
                    foreach (char s in Game_Managment.slova)
                    {
                        if (s.ToString() == slovo)
                            c++;
                    }
                }
                if (c >= 3)
                {
                    c = 0;
                    slovo = ostala_slova[UnityEngine.Random.Range(0, 17)].ToString();
                }
                else { break; }
            }
        }

        return slovo;
    }

    public void GenerirajNovaSlova()
    {
        this.GetComponent<AudioSource>().clip = sound_nova_slova;
        this.GetComponent<AudioSource>().Play();
        brojac = 0;
        bonusAttack = 0;
        if (PlayerData.credits >= 100)
        {
            foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn1.name.Contains("BTN_"))
                {
                    Destroy(btn1);
                    Destroy(btn1.gameObject);
                }
            }
            btn_attack.interactable = false;
            PlayerData.credits -= 100;
            GenerirajSlova();
        }
        else
        {
            lbl_money.color = Color.red;
            StartCoroutine(WaitCoupleSec());
        }

        gm.refreshStats();

    }
    IEnumerator WaitCoupleSec()
    {
        yield return new WaitForSeconds(4);
        lbl_money.color = Color.white;
    }

}
