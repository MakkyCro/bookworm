﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.SceneManagement;
using System;

public class MenuScript : MonoBehaviour
{
    public AudioClip sound_click;

    public GameObject MainMenuCanvas;
    public GameObject panel_mainMenu;
    public GameObject panel_OdabirModa;
    public GameObject panel_Opcije;
    public GameObject panel_trgovina;
    public GameObject panel_Oigri;
    public GameObject panel_login_Logo;
    public GameObject panel_postignuca;
    public GameObject panel_mapa;

    public TMP_Dropdown cb_usernames;
    public TMP_InputField tb_login;
    public TextMeshProUGUI lbl_naslov;
    public Button btn_prijaviSe;
    public Button btn_NapraviNovi;
    public Button btn_OdjaviSe;
    public Button lvl1;
    public Button lvl2;
    public Button lvl3;
    public Button lvl4;
    public Button lvl5;
    public Button lvl6;
    public Button lvl7;
    public Button lvl8;
    public Text lbl_red;
    public Text lbl_green;
    public Text lbl_orange;
    public Text lbl_blue;

    public LoadingData ld;
    public TextAsset file_rijeci;


    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    public static string usernameLogin = "";
    public static int lvl = 0;
    void Start()
    {
        ld = GameObject.FindObjectOfType(typeof(LoadingData)) as LoadingData;
        if (usernameLogin == "")
        {
            btn_OdjaviSe.interactable = false;
            cb_usernames.interactable = true;
            btn_prijaviSe.interactable = true;
            btn_NapraviNovi.interactable = true;
            tb_login.enabled = true;
            panel_mainMenu.SetActive(false);
        }
        else
        {
            btn_OdjaviSe.interactable = true;
            cb_usernames.interactable = false;
            btn_prijaviSe.interactable = false;
            btn_NapraviNovi.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            RefreshData();
        }
       
        try
        {
            System.IO.Directory.CreateDirectory(pathDocuments + "\\Bookworm");
        }
        catch { }

        if (!File.Exists(pathDocuments + "\\Bookworm\\imeniceBookWorm.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\imeniceBookWorm.txt")) { }

            File.WriteAllText(pathDocuments + "\\Bookworm\\imeniceBookWorm.txt", file_rijeci.text);
        }
        RefreshDropDown();
    }

    public void TimeAttackPressed()
    {
        this.GetComponent<AudioSource>().Play();
        AutoFade.LoadLevel("TimeAttack", 1, 1, Color.black);
    }
    public void AvanturaPressed()
    {
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.lvl1)
            lvl1.interactable = true;
        else
            lvl1.interactable = false;

        if (PlayerData.lvl2)
            lvl2.interactable = true;
        else
            lvl2.interactable = false;

        if (PlayerData.lvl3)
            lvl3.interactable = true;
        else
            lvl3.interactable = false;

        if (PlayerData.lvl4)
            lvl4.interactable = true;
        else
            lvl4.interactable = false;

        if (PlayerData.lvl5)
            lvl5.interactable = true;
        else
            lvl5.interactable = false;

        if (PlayerData.lvl6)
            lvl6.interactable = true;
        else
            lvl6.interactable = false;

        if (PlayerData.lvl7)
            lvl7.interactable = true;
        else
            lvl7.interactable = false;

        if (PlayerData.lvl8)
            lvl8.interactable = true;
        else
            lvl8.interactable = false;

        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_login_Logo.SetActive(false);
        panel_postignuca.SetActive(false);
        panel_mapa.SetActive(true);
        lbl_naslov.text = "";
    }

    public void LVL1Pressed()
    {
        lvl = 1;
        ScenaAvantura();
    }
    public void LVL2Pressed()
    {
        lvl = 2;
        ScenaAvantura();
    }
    public void LVL3Pressed()
    {
        lvl = 3;
        ScenaAvantura();
    }
    public void LVL4Pressed()
    {
        lvl = 4;
        ScenaAvantura();
    }
    public void LVL5Pressed()
    {
        lvl = 5;
        ScenaAvantura();
    }
    public void LVL6Pressed()
    {
        lvl = 6;
        ScenaAvantura();
    }
    public void LVL7Pressed()
    {
        lvl = 7;
        ScenaAvantura();
    }
    public void LVL8Pressed()
    {
        lvl = 8;
        ScenaAvantura();
    }

    void ScenaAvantura()
    {
        this.GetComponent<AudioSource>().Play();
        AutoFade.LoadLevel("BattleScene",1,1,Color.black);
    }
    public void Odjava()
    {
        this.GetComponent<AudioSource>().Play();
        btn_OdjaviSe.interactable = false;
        cb_usernames.interactable = true;
        btn_prijaviSe.interactable = true;
        btn_NapraviNovi.interactable = true;
        tb_login.enabled = true;
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_postignuca.SetActive(false);
        panel_mapa.SetActive(false);
        panel_Oigri.SetActive(false);
        usernameLogin = "";
        RefreshDropDown();
        tb_login.text = "";
        lbl_naslov.text = "Glavni izbornik";
        //cb_usernames.options[cb_usernames.value].text = "";
    }

    bool postoji()
    {
        var usernames = File.ReadAllLines(pathDocuments + "\\Bookworm\\usernames.txt");
        var lista = new List<string>(usernames);
        bool postoji = false;

        foreach (string username in lista)
        {
            if (tb_login.text == username)
            {
                postoji = true;
                break;
            }
            else { postoji = false; }
        }

        return postoji;
    }

    public void PrijaviSe()
    {
        this.GetComponent<AudioSource>().Play();

        if (tb_login.text=="" && cb_usernames.options[cb_usernames.value].text != "")
        {
            tb_login.text = cb_usernames.options[cb_usernames.value].text;
        }
        if (tb_login.text != "" && postoji())
        {
            cb_usernames.interactable = false;
            btn_OdjaviSe.interactable = true;
            btn_prijaviSe.interactable = false;
            btn_NapraviNovi.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            usernameLogin = tb_login.text;
            lbl_naslov.text = "Glavni izbornik";
            RefreshData();
        }
        else
        {
            Debug.Log("Ne postoji taj username!");
        }

    }

    public void NapraviNoviProfil()
    {
        this.GetComponent<AudioSource>().Play();
        if (!postoji() && tb_login.text != "")
        {
            File.AppendAllText(pathDocuments + "\\Bookworm\\usernames.txt", tb_login.text+"\n");
            PlayerData.username = tb_login.text;
            usernameLogin = PlayerData.username;
            cb_usernames.interactable = false;
            btn_OdjaviSe.interactable = true;
            btn_prijaviSe.interactable = false;
            btn_NapraviNovi.interactable = false;
            tb_login.enabled = false;
            panel_mainMenu.SetActive(true);
            usernameLogin = tb_login.text;
            RefreshData();
        }
        else
        {
            Debug.Log("Postoji vec!");
        }
       // RefreshDropDown();
    }

    public void CopyValueToTB_login()
    {
        tb_login.text = cb_usernames.options[cb_usernames.value].text;
    }

    void RefreshDropDown()
    {
        if (!File.Exists(pathDocuments + "\\Bookworm\\usernames.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\usernames.txt")) { }
        }

        if (new FileInfo(pathDocuments + "\\Bookworm\\usernames.txt").Length != 0)
        {
            cb_usernames.interactable = true;
            var usernames = File.ReadAllLines(pathDocuments + "\\Bookworm\\usernames.txt");
            List<string> lista = new List<string>(usernames);
            //cb_usernames = GetComponent<TMP_Dropdown>();
            cb_usernames.options.Clear();
            foreach (string t in lista)
            {
                cb_usernames.options.Add(new TMP_Dropdown.OptionData() { text = t });
            }
            cb_usernames.RefreshShownValue();
        }
        else { cb_usernames.interactable = false; }
    }
    
    void RefreshData()
    {
        ld.LoadData();
        ld.lbl_credits.text = PlayerData.credits.ToString();

        lbl_red.text = "x"+PlayerData.red_potions;
        lbl_green.text = "x" + PlayerData.green_potions;
        lbl_orange.text = "x" + PlayerData.orange_potions;
        lbl_blue.text = "x" + PlayerData.blue_potions;
    }

    void saveChange()
    {
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", 
                     PlayerData.username
            + "\n" + PlayerData.xp
            + "\n" + PlayerData.LVL
            + "\n" + PlayerData.c_health
            + "\n" + PlayerData.m_health
            + "\n" + PlayerData.e_c_health
            + "\n" + PlayerData.e_m_health
            + "\n" + PlayerData.BaseAttackDmg 
            + "\n" + PlayerData.credits 
            + "\n" + PlayerData.red_potions 
            + "\n" + PlayerData.green_potions 
            + "\n" + PlayerData.orange_potions 
            + "\n" + PlayerData.blue_potions 
            + "\n" + PlayerData.BonusAttack 
            + "\n" + PlayerData.difficulty 
            + "\n" + PlayerData.lvl1 
            + "\n" + PlayerData.lvl2 
            + "\n" + PlayerData.lvl3 
            + "\n" + PlayerData.lvl4 
            + "\n" + PlayerData.lvl5 
            + "\n" + PlayerData.lvl6 
            + "\n" + PlayerData.lvl7 
            + "\n" + PlayerData.lvl8 
            + "\n");

        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt",
                        PlayerData.rijeci10
               + "\n" + PlayerData.rijeci25
               + "\n" + PlayerData.rijeci50
               + "\n" + PlayerData.rijeci100
               + "\n" + PlayerData.rijeci250
               + "\n" + PlayerData.rijeci500
               + "\n" + PlayerData.rijeci1000
               + "\n" + PlayerData.rijeci10_2
               + "\n" + PlayerData.rijeci25_2
               + "\n" + PlayerData.rijeci50_2
               + "\n" + PlayerData.slova5
               + "\n" + PlayerData.slova6
               + "\n" + PlayerData.slova7
               + "\n" + PlayerData.slova8
               + "\n" + PlayerData.slova9
               + "\n" + PlayerData.slova10);
    }

    public void KupiRedPotion()
    {
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 50)
        {
            PlayerData.red_potions += 1;
            PlayerData.credits -= 50;
            saveChange();
            RefreshData();
        }
    }
    public void KupiGreenPotion()
    {
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 75)
        {
            PlayerData.green_potions += 1;
            PlayerData.credits -= 75;
            saveChange();
            RefreshData();
        }
    }
    public void KupiOrangePotion()
    {
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 100)
        {
            PlayerData.orange_potions += 1;
            PlayerData.credits -= 100;
            saveChange();
            RefreshData();
        }
    }
    public void KupiBluePotion()
    {
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 125)
        {
            PlayerData.blue_potions += 1;
            PlayerData.credits -= 125;
            saveChange();
            RefreshData();
        }
    }

    public void PostignucaPressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_login_Logo.SetActive(false);
        panel_postignuca.SetActive(true);
        if (PlayerData.rijeci10)
            GameObject.Find("btn_10").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_10").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci25)
            GameObject.Find("btn_25").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_25").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci50)
            GameObject.Find("btn_50").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_50").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci100)
            GameObject.Find("btn_100").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_100").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci250)
            GameObject.Find("btn_250").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_250").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci500)
            GameObject.Find("btn_500").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_500").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci1000)
            GameObject.Find("btn_1000").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_1000").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci10_2)
            GameObject.Find("btn_10r").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_10r").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci25_2)
            GameObject.Find("btn_25r").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_25r").GetComponent<Button>().interactable = false;

        if (PlayerData.rijeci50_2)
            GameObject.Find("btn_50r").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_50r").GetComponent<Button>().interactable = false;

        if (PlayerData.slova5)
            GameObject.Find("btn_5s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_5s").GetComponent<Button>().interactable = false;

        if (PlayerData.slova6)
            GameObject.Find("btn_6s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_6s").GetComponent<Button>().interactable = false;

        if (PlayerData.slova7)
            GameObject.Find("btn_7s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_7s").GetComponent<Button>().interactable = false;

        if (PlayerData.slova8)
            GameObject.Find("btn_8s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_8s").GetComponent<Button>().interactable = false;

        if (PlayerData.slova9)
            GameObject.Find("btn_9s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_9s").GetComponent<Button>().interactable = false;

        if (PlayerData.slova10)
            GameObject.Find("btn_10s").GetComponent<Button>().interactable = true;
        else
            GameObject.Find("btn_10s").GetComponent<Button>().interactable = false;

        lbl_naslov.text = "";
    }

    public void OigriPressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_Oigri.SetActive(true);
        panel_postignuca.SetActive(false);
        lbl_naslov.text = "O igri";
    }

    public void TrgovinaPressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(true);
        panel_Oigri.SetActive(false);
        panel_login_Logo.SetActive(false);
        panel_postignuca.SetActive(false);
        lbl_naslov.text = "Trgovina";
    }

    public void OpcijePressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(true);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_postignuca.SetActive(false);
        panel_Oigri.SetActive(false);
        lbl_naslov.text = "Opcije";
    }

    public void OdabirModaPressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(false);
        panel_OdabirModa.SetActive(true);
        panel_Opcije.SetActive(false);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_postignuca.SetActive(false);
        panel_Oigri.SetActive(false);
        lbl_naslov.text = "Odaberi mod igranja";
    }

    public void nazadPressed()
    {
        this.GetComponent<AudioSource>().Play();
        panel_mainMenu.SetActive(true);
        panel_OdabirModa.SetActive(false);
        panel_Opcije.SetActive(false);
        panel_mapa.SetActive(false);
        panel_trgovina.SetActive(false);
        panel_Oigri.SetActive(false);
        panel_postignuca.SetActive(false);
        panel_login_Logo.SetActive(true);
        lbl_naslov.text = "Glavni izbornik";
    }

    public void izlazPressed()
    {
        this.GetComponent<AudioSource>().Play();
        if (!Application.isEditor) System.Diagnostics.Process.GetCurrentProcess().Kill();
    }
}
