﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideGameObject : MonoBehaviour
{
    public GameObject obj;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && obj.activeSelf)
        {
            obj.SetActive(false);
            if (obj.name == "congratulationsCanvas")
                AutoFade.LoadLevel("BattleScene", 1, 1, Color.black);
            else if(obj.name == "congratulationsCanvasTA" || obj.name == "victoryCanvas")
            {
                Time.timeScale = 1f;
                AutoFade.LoadLevel("MainMenu", 1, 1, Color.black);
            }
            else
                Time.timeScale = 1f;
        }
    }
}
