﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class PotionsTextManagment : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject childText = null; //  or make public and drag
    Text text;
    TextMeshProUGUI tmPro;
    void Start()
    {
        
        try
        {
            text = GetComponentInChildren<Text>();
        }
        catch { }
        try
        {
            tmPro = GetComponentInChildren<TextMeshProUGUI>();
        }
        catch { }

        if (text != null)
        {
            childText = text.gameObject;
            if(text.name != "opis")
                childText.SetActive(false);
        }
        if (tmPro != null)
        {
            childText = tmPro.gameObject;
            childText.SetActive(false);
        }
    }
    public void OnPointerEnter(UnityEngine.EventSystems.PointerEventData eventData)
    {
        childText.SetActive(true);
    }
    public void OnPointerExit(UnityEngine.EventSystems.PointerEventData eventData)
    {
        childText.SetActive(false);
    }

}
