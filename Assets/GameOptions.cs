﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class GameOptions : MonoBehaviour
{
    public Slider slider_volume;
    public Toggle toggle_fullscreen;
    public Dropdown dropdown_resolution;
    public AudioMixer am;

    Resolution[] resolutions;

    private void Start()
    {
        resolutions = Screen.resolutions;

        dropdown_resolution.ClearOptions();

        List<string> r_options = new List<string>();

        int currentResolutionIndex = 0;
        for(int i = 0; i < resolutions.Length; i++)
        {
            string option_title = resolutions[i].width + " x " + resolutions[i].height;
            r_options.Add(option_title);
            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        dropdown_resolution.AddOptions(r_options);
        dropdown_resolution.value = currentResolutionIndex;
        dropdown_resolution.RefreshShownValue();
    }

    public void setResolution(int resolutionIndex)
    {
        Resolution res = resolutions[resolutionIndex];
        Screen.SetResolution(res.width, res.height,Screen.fullScreen);
    }
    public void SetVolume(float volume)
    {
        am.SetFloat("volume", volume);
    }

    public void toggleFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

}
