﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Postignuca : MonoBehaviour
{
    public Text tb_opis;

    public void Rijeci_10_Enter()
    {
        tb_opis.text = "Naziv medalje: Novak\n\nOpis: pronađeno 10 različitih riječi.";
    }
    public void Rijeci_25_Enter()
    {
        tb_opis.text = "Naziv medalje: Početnik\n\nOpis: pronađeno 25 različitih riječi.";
    }
    public void Rijeci_50_Enter()
    {
        tb_opis.text = "Naziv medalje: Talentiran\n\nOpis: pronađeno 50 različitih riječi.";
    }
    public void Rijeci_100_Enter()
    {
        tb_opis.text = "Naziv medalje: Vješt\n\nOpis: pronađeno 100 različitih riječi.";
    }
    public void Rijeci_250_Enter()
    {
        tb_opis.text = "Naziv medalje: Iskusan\n\nOpis: pronađeno 250 različitih riječi.";
    }
    public void Rijeci_500_Enter()
    {
        tb_opis.text = "Naziv medalje: Napredan\n\nOpis: pronađeno 500 različitih riječi.";
    }
    public void Rijeci_1000_Enter()
    {
        tb_opis.text = "Naziv medalje: Stručnjak\n\nOpis: pronađeno 1000+ različitih riječi.";
    }

    public void Slova_5_Enter()
    {
        tb_opis.text = "Naziv medalje: Predškolac\n\nOpis: napravi riječ od 5 slova.";
    }
    public void Slova_6_Enter()
    {
        tb_opis.text = "Naziv medalje: Osnovnoškolac\n\nOpis: napravi riječ od 6 slova.";
    }
    public void Slova_7_Enter()
    {
        tb_opis.text = "Naziv medalje: Srednjoškolac\n\nOpis: napravi riječ od 7 slova.";
    }
    public void Slova_8_Enter()
    {
        tb_opis.text = "Naziv medalje: Student\n\nOpis: napravi riječ od 8 slova.";
    }
    public void Slova_9_Enter()
    {
        tb_opis.text = "Naziv medalje: Učitelj\n\nOpis: napravi riječ od 9 slova.";
    }
    public void Slova_10_Enter()
    {
        tb_opis.text = "Naziv medalje: Profesor\n\nOpis: napravi riječ od 10+ slova.";
    }
    public void Rijeci_10_2_Enter()
    {
        tb_opis.text = "Naziv medalje: Puž\n\nOpis: pronađi 10 riječi u modu isteka vremena.";
    }
    public void Rijeci_25_2_Enter()
    {
        tb_opis.text = "Naziv medalje: Slon\n\nOpis: pronađi 25 riječi u modu isteka vremena.";
    }
    public void Rijeci_50_2_Enter()
    {
        tb_opis.text = "Naziv medalje: Gepard\n\nOpis: pronađi 50+ riječi u modu isteka vremena.";
    }
    public void CursorLeave()
    {
        tb_opis.text = "";
    }
}
