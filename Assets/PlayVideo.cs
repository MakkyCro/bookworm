﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayVideo : MonoBehaviour
{
    public GameObject MenuCanvas;
    public VideoClip intro;

    // Start is called before the first frame update
    bool isPlaying = false;
    VideoPlayer VP;
    void Update()
    {
        if ((Input.GetKeyDown("escape") || Input.GetKeyDown("space") || Input.GetMouseButtonDown(0)) && isPlaying)
        {
            VP.frame = 200;
            isPlaying = false;
            MenuCanvas.SetActive(true);
        }
    }
    void Start()
    {
        GameObject camera = GameObject.Find("Main Camera");
        var videoPlayer = camera.AddComponent<UnityEngine.Video.VideoPlayer>();
        VP = videoPlayer;
        videoPlayer.playOnAwake = true;
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraFarPlane;
        //videoPlayer.targetCameraAlpha = 0.5F;
        //videoPlayer.url = "/Users/graham/movie.mov";
        videoPlayer.clip = intro;
        //videoPlayer.frame = 100;
        videoPlayer.isLooping = false;
        videoPlayer.loopPointReached += EndReached;
        videoPlayer.Play();
        isPlaying = true;
    }
    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        isPlaying = false;
        MenuCanvas.SetActive(true);
    }

}
