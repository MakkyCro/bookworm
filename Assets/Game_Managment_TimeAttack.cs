﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
public class Game_Managment_TimeAttack : MonoBehaviour
{
    public GridLayoutGroup layoutGeneratedLetters;
    public GridLayoutGroup layoutPressedLetters;

    public Button button;
    public Button btn_add;
    public Button btn_resetiraj;
    public Button btn_izmjesaj;
    public Button btn_orange_potion;
    public Button btn_blue_potion;
    public Button btn_nova_slova;

    public TextMeshProUGUI lbl_money;
    public TextMeshProUGUI lbl_xp;
    public TextMeshProUGUI lbl_xp_result;
    public TextMeshProUGUI lbl_player_name;
    public TextMeshProUGUI lbl_timeLeft;
    public TextMeshProUGUI lbl_lvlUp_xp;

    public Text lbl_num_orange_potions;
    public Text lbl_num_blue_potions;
    public Text lbl_stats;
    public Text lbl_rijeci;
    public Text lbl_name_result;
    public Text lbl_new_words_result;
    public Text lbl_odbrojavanje;
    public Text lbl_br_rijeci;
    public Text lbl_lvlUp_lvl;

    public GameObject resultCanvas;
    public GameObject lvlUpCanvas;
    public GameObject bg;
    public GameObject usporiVrijemeAnimacija;
    public GameObject dodajVrijemeAnimacija;

    public AudioClip sound_potion;
    public AudioClip sound_add;
    public AudioClip sound_izmjesaj;
    public AudioClip sound_resetiraj;

    public Slider slider_timeLeft;
    public Slider slider_xp;
    public Slider slider_xp_result;
    public Slider slider_xp_LVLup;

    public Sprite bg1;
    public GeneratorSlovaTimeAttack gs_TimeAttack;

    char[] samoglasnici = { 'A', 'E', 'I', 'O', 'U' };
    char[] ostala_slova = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'V', 'Z' };
    char[] rijetka_slova = { 'Č', 'Ć', 'Đ', 'Š', 'Ž' };

    public static List<char> slova = new List<char>();
    public static string word;
    public static string[] rijeci;
    public static List<string> spremljene_rijeci = new List<string>();

    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    int[] LVL_borders = { 50, 125, 300, 500, 750 };

    int baseDmgByLVL;
    int bonusVrijeme = 0;
    int timeLeft = 180;
    int odbrojavanje10s = 10;
    int br_rijeci = 0;
    int lvl = 0;

    bool usporeno = false;
    bool svakadruga = false;
    void Start()
    {
        bg.GetComponent<Image>().sprite = bg1;
        br_rijeci = 0;
        rijeci = File.ReadAllLines(pathDocuments + "\\Bookworm\\imeniceBookWorm.txt");

        InvokeRepeating("odbrojavanje",0,1);
        StartCoroutine(odbrojavanjePocetka());

        btn_blue_potion.interactable = false;
        btn_orange_potion.interactable = false;
        btn_izmjesaj.interactable = false;
        btn_resetiraj.interactable = false;
        btn_add.gameObject.SetActive(false);
        btn_add.interactable = false;
        btn_nova_slova.interactable = false;


        gs_TimeAttack = GameObject.FindObjectOfType(typeof(GeneratorSlovaTimeAttack)) as GeneratorSlovaTimeAttack;
        gs_TimeAttack.GenerirajSlova();

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("BTN"))
            {
                btn1.interactable = false;
            }
        }

        CalculateBaseAttack();
        lbl_name_result.text = PlayerData.username;
        
        refreshStats();
        lvl = CalculateLVL(PlayerData.xp);
    }
    private void Awake()
    {
        slider_timeLeft.maxValue = timeLeft;
        slider_timeLeft.value = slider_timeLeft.maxValue;
        lbl_timeLeft.text = timeLeft.ToString() + " s";
    }

    public void DodajVrijeme()
    {
        lvl = CalculateLVL(PlayerData.xp);

        this.GetComponent<AudioSource>().clip = sound_add;
        this.GetComponent<AudioSource>().Play();

        br_rijeci++;
        btn_add.interactable = false;
        Obrisi(true);
        gs_TimeAttack.brojac = 0;

        string rijec = "";
        bonusVrijeme = 0;

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("Copy") && !btn1.name.Contains("Green") && !btn1.name.Contains("Red"))
            {
                bonusVrijeme += 1;
                rijec += btn1.name.Split('_')[2];
            }
            else if (btn1.name.Contains("Copy") && btn1.name.Contains("Green"))
            {
                bonusVrijeme += 2;
                rijec += btn1.name.Split('_')[2];
            }
            else if (btn1.name.Contains("Copy") && btn1.name.Contains("Red"))
            {
                bonusVrijeme += 5;
                rijec += btn1.name.Split('_')[2];
            }
        }

        rijec = Reverse(rijec);
        spremljene_rijeci.Add(rijec);
        lbl_rijeci.text += rijec + " ";
        SpremiRijec(rijec);
        DodajNovuRijecResult(rijec);

        timeLeft += bonusVrijeme;
        if (timeLeft > slider_timeLeft.maxValue)
        {
            slider_timeLeft.maxValue = timeLeft;
        }
        slider_timeLeft.value = timeLeft;

        lbl_timeLeft.text = timeLeft.ToString() + " s";
        PlayerData.credits += bonusVrijeme;
        PlayerData.xp += bonusVrijeme;
        CheckForAchivement(rijec);
        refreshStats();
        SaveProgress();
        CheckForLVLup(lvl);
    }
    void CheckForLVLup(int lvl)
    {
        int tmp = CalculateLVL(PlayerData.xp);
        if (tmp != lvl)
        {
            Time.timeScale = 0f;
            lvlUpCanvas.SetActive(true);
        }
    }

    void CheckForAchivement(string rijec)
    {
        var data = File.ReadAllLines(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt");
        var lista = new List<string>(data);

        if (br_rijeci == 10 && !PlayerData.rijeci10_2)
            PlayerData.rijeci10_2 = true;
        if (br_rijeci == 25 && !PlayerData.rijeci25_2)
            PlayerData.rijeci25_2 = true;
        if (br_rijeci == 50 && !PlayerData.rijeci50_2)
            PlayerData.rijeci50_2 = true;

        if (lista.Count == 10 && !PlayerData.rijeci10)
            PlayerData.rijeci10 = true;
        if (lista.Count == 25 && !PlayerData.rijeci25)
            PlayerData.rijeci25 = true;
        if (lista.Count == 50 && !PlayerData.rijeci50)
            PlayerData.rijeci50 = true;
        if (lista.Count == 100 && !PlayerData.rijeci100)
            PlayerData.rijeci100 = true;
        if (lista.Count == 250 && !PlayerData.rijeci250)
            PlayerData.rijeci250 = true;
        if (lista.Count == 500 && !PlayerData.rijeci500)
            PlayerData.rijeci500 = true;
        if (lista.Count == 1000 && !PlayerData.rijeci1000)
            PlayerData.rijeci1000 = true;

        if (rijec.Length == 5 && !PlayerData.slova5)
            PlayerData.slova5 = true;
        if (rijec.Length == 6 && !PlayerData.slova6)
            PlayerData.slova6 = true;
        if (rijec.Length == 7 && !PlayerData.slova7)
            PlayerData.slova7 = true;
        if (rijec.Length == 8 && !PlayerData.slova8)
            PlayerData.slova8 = true;
        if (rijec.Length == 9 && !PlayerData.slova9)
            PlayerData.slova9 = true;
        if (rijec.Length >= 10 && !PlayerData.slova10)
            PlayerData.slova10 = true;
    }
    public void DodajNovuRijecResult(string unos)
    {
        bool postoji = false;
        //string[] rijeci = File.ReadAllLines(pathDocuments+ "\\Bookworm\\imeniceBookWorm.txt");
        foreach (string r in rijeci)
        {
            if (r == unos)
            {
                postoji = true;
                break;
            }
        }
        if (!postoji)
        {
            lbl_new_words_result.text += unos + "\t";
        }
    }
    public void SpremiRijec(string rijec)
    {
        if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt")) { }
        }
        File.AppendAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt", rijec + "\n");
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
    public void refreshStats()
    {
       /* lbl_e_health.text = PlayerData.e_c_health.ToString();
        slider_p_health.maxValue = PlayerData.m_health;
        slider_p_health.value = PlayerData.c_health;
        slider_e_health.maxValue = PlayerData.e_m_health;
        slider_e_health.value = PlayerData.e_c_health;*/
        
        lbl_num_orange_potions.text = "x " + PlayerData.orange_potions.ToString();
        lbl_num_blue_potions.text = "x " + PlayerData.blue_potions.ToString();

        lbl_xp.text = PlayerData.xp + " xp / " + CalculateXP(PlayerData.xp) + " xp (" + CalculateLVL(PlayerData.xp) + " LVL)";
        slider_xp.maxValue = CalculateXP(PlayerData.xp);
        slider_xp.value = PlayerData.xp;

        lbl_xp_result.text = PlayerData.xp + " xp / " + CalculateXP(PlayerData.xp) + " xp (" + CalculateLVL(PlayerData.xp) + " LVL)";
        slider_xp_result.maxValue = CalculateXP(PlayerData.xp);
        slider_xp_result.value = PlayerData.xp;

        lbl_money.text = PlayerData.credits.ToString();
        //lbl_stats.text = "Temeljni napad: " + PlayerData.BaseAttackDmg + "\nDodatan napad od zadnje riječi: " + bonusAttack + "\nProsječan broj iskorištenih slova po riječi: " + ProsjekSlova().ToString("F2");

        lbl_player_name.text = PlayerData.username;

        lbl_lvlUp_lvl.text = CalculateLVL(PlayerData.xp).ToString();
        lbl_lvlUp_xp.text = PlayerData.xp + " xp / " + CalculateXP(PlayerData.xp) + " xp";
        slider_xp_LVLup.maxValue = CalculateXP(PlayerData.xp);
        slider_xp_LVLup.value = PlayerData.xp;
    }
    

    void odbrojavanje()
    {
        if(odbrojavanje10s > 0)
        {
            lbl_odbrojavanje.text = odbrojavanje10s.ToString();
            odbrojavanje10s -= 1;
        }
        else
        {
            lbl_odbrojavanje.text = "Kreni!";
            StartCoroutine(makniOdbrojavanje());
        }
    }

    void refreshTime()
    {
        if(timeLeft > 0 && !usporeno)
        {
            timeLeft -= 1;
            slider_timeLeft.value = timeLeft;
            lbl_timeLeft.text = timeLeft.ToString() + " s";
            slider_timeLeft.value = timeLeft;

        }
        else if( timeLeft > 0 && usporeno)
        {
            if(!svakadruga)
            {
                svakadruga = true;
            }else
            {
                timeLeft -= 1;
                svakadruga = false;
            }
            slider_timeLeft.value = timeLeft;
            lbl_timeLeft.text = timeLeft.ToString() + " s";
            slider_timeLeft.value = timeLeft;
        }
        else
        {
            lbl_timeLeft.text = "Vrijeme je isteklo!";

            CancelInvoke();

            lbl_br_rijeci.text = br_rijeci.ToString();
            resultCanvas.SetActive(true);
            Time.timeScale = 0f;
        }

    }

    public void SaveProgress()
    {
        if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt")) { }
        }
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.xp + "\n" + PlayerData.LVL + "\n" + PlayerData.c_health + "\n" + PlayerData.m_health + "\n" + PlayerData.e_c_health + "\n" + PlayerData.e_m_health + "\n" +
                PlayerData.BaseAttackDmg + "\n" + PlayerData.credits + "\n" + PlayerData.red_potions + "\n" + PlayerData.green_potions + "\n" + PlayerData.orange_potions + "\n" + PlayerData.blue_potions + "\n" + PlayerData.BonusAttack + "\n" + PlayerData.difficulty + "\n" + PlayerData.lvl1 + "\n" + PlayerData.lvl2 + "\n" + PlayerData.lvl3 + "\n" + PlayerData.lvl4 + "\n" + PlayerData.lvl5 + "\n"
                 + PlayerData.lvl6 + "\n" + PlayerData.lvl7 + "\n" + PlayerData.lvl8 + "\n");

        if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt")) { }
        }
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt",
                      PlayerData.rijeci10
             + "\n" + PlayerData.rijeci25
             + "\n" + PlayerData.rijeci50
             + "\n" + PlayerData.rijeci100
             + "\n" + PlayerData.rijeci250
             + "\n" + PlayerData.rijeci500
             + "\n" + PlayerData.rijeci1000
             + "\n" + PlayerData.rijeci10_2
             + "\n" + PlayerData.rijeci25_2
             + "\n" + PlayerData.rijeci50_2
             + "\n" + PlayerData.slova5
             + "\n" + PlayerData.slova6
             + "\n" + PlayerData.slova7
             + "\n" + PlayerData.slova8
             + "\n" + PlayerData.slova9
             + "\n" + PlayerData.slova10);
    }

    public void UseBluePotion()
    {
        if (PlayerData.orange_potions > 0 && timeLeft > 0)
        {
            this.GetComponent<AudioSource>().clip = sound_potion;
            this.GetComponent<AudioSource>().Play();

            timeLeft += 30;
            if(timeLeft > slider_timeLeft.maxValue)
            {
                slider_timeLeft.maxValue = timeLeft;
            }
            slider_timeLeft.value = timeLeft;

            lbl_timeLeft.text = timeLeft.ToString() + " s";

            btn_orange_potion.interactable = false;
            btn_blue_potion.interactable = false;

            StartCoroutine(cooldownPotions());
            Instantiate(dodajVrijemeAnimacija, new Vector3(-5.33f, -0.8f, 0), Quaternion.identity);
        }
        refreshStats();
        SaveProgress();
    }

    public void UseOrangePotion()
    {
        if (PlayerData.blue_potions > 0)
        {
            this.GetComponent<AudioSource>().clip = sound_potion;
            this.GetComponent<AudioSource>().Play();

            btn_blue_potion.interactable = false;
            btn_orange_potion.interactable = false;
            usporeno = true;
            StartCoroutine(ubrzajVrijeme());
            StartCoroutine(cooldownPotions());
            Instantiate(usporiVrijemeAnimacija, new Vector3(0f, 0f, 0), Quaternion.identity);
        }
        refreshStats();
        SaveProgress();
    }
    public int CalculateLVL(int xp)
    {
        int LVL = 1;
        foreach (int max_xp in LVL_borders)
        {
            if (xp >= max_xp)
            {
                LVL++;
            }
            else { break; }
        }
        PlayerData.LVL = LVL;
        return LVL;
    }

    public int CalculateXP(int xp)
    {
        int xp_nextLVL = 100;
        foreach (int max_xp in LVL_borders)
        {
            if (xp >= max_xp)
            {
                xp_nextLVL = max_xp;
            }
            else
            {
                xp_nextLVL = max_xp;
                break;
            }
        }
        return xp_nextLVL;
    }
    public int CalculateBaseAttack()
    {
        baseDmgByLVL = CalculateLVL(PlayerData.xp) * 10;
        PlayerData.BaseAttackDmg = baseDmgByLVL + Convert.ToInt32(PlayerData.xp / 10);
        return PlayerData.BaseAttackDmg;
    }

    public void Izmjesaj()
    {
        Obrisi(true);
        gs_TimeAttack.brojac = 0;

        this.GetComponent<AudioSource>().clip = sound_izmjesaj;
        this.GetComponent<AudioSource>().Play();


        List<string> generirana_slova = new List<string>();
        int i = 0;

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (!btn1.name.Contains("Copy") && !btn1.name.Contains("Attack") && !btn1.name.Contains("Randomize") && !btn1.name.Contains("Clear") && btn1.name.Contains("BTN_"))
            {
                generirana_slova.Add(btn1.name.Split('_')[0] + "_" + btn1.name.Split('_')[1]);
                Destroy(btn1);
                Destroy(btn1.gameObject);
            }
        }

        for (var j = generirana_slova.Count - 1; j > 0; j--)
        {
            var r = UnityEngine.Random.Range(0, j);
            var tmp = generirana_slova[j];
            generirana_slova[j] = generirana_slova[r];
            generirana_slova[r] = tmp;
        }

        foreach (string slovo in generirana_slova)
        {
            //Debug.Log(slovo);
            Button newButton = Instantiate(button) as Button;
            newButton.name = "BTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            if (slovo.Contains("Green"))
            {
                newButton.GetComponentInChildren<Text>().color = Color.green;
                newButton.name = "GreenBTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            else if (slovo.Contains("Red"))
            {
                newButton.GetComponentInChildren<Text>().color = Color.red;
                newButton.name = "RedBTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            else
            {
                newButton.GetComponentInChildren<Text>().color = Color.black;
                newButton.name = "BTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            newButton.GetComponentInChildren<Text>().text = generirana_slova[i].Split('_')[1];
            newButton.GetComponent<Button>().onClick.AddListener(delegate { gs_TimeAttack.button_Click(newButton); });
            newButton.transform.SetParent(layoutGeneratedLetters.transform, false);
            i++;
        }
    }
    public void Obrisi(bool temp = false)
    {
        if(!temp)
        {
            this.GetComponent<AudioSource>().clip = sound_resetiraj;
            this.GetComponent<AudioSource>().Play();
        }
        gs_TimeAttack.brojac = 0;
        try
        {
            foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn1.name.Contains("Copy"))
                {
                    Destroy(btn1);
                    Destroy(btn1.gameObject);
                }
                else
                {
                    if (btn1.name != "Bttn_Add" && !btn1.name.Contains("orangePotion") && !btn1.name.Contains("bluePotion"))
                    { btn1.interactable = true; }
                    else if (!btn1.name.Contains("orangePotion") && !btn1.name.Contains("bluePotion"))
                    { btn1.interactable = false; }
                }
            }
        }
        catch { Debug.Log("Već je resetirano!"); }
    }

    IEnumerator odbrojavanjePocetka()
    {
        yield return new WaitForSeconds(10);
        CancelInvoke();
        InvokeRepeating("refreshTime", 1.0f, 1.0f);
        btn_blue_potion.interactable = true;
        btn_orange_potion.interactable = true;
        btn_izmjesaj.interactable = true;
        btn_resetiraj.interactable = true;
        btn_nova_slova.interactable = true;
        btn_add.gameObject.SetActive(true);
        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("BTN"))
            {
                btn1.interactable = true;
            }
        }
    }
    IEnumerator ubrzajVrijeme()
    {
        yield return new WaitForSeconds(10);
        usporeno = false;
        Destroy(GameObject.Find("CFX2_Wandering_Spirits(Clone)"));
    }

    IEnumerator makniOdbrojavanje()
    {
        yield return new WaitForSeconds(1.5f);
        lbl_odbrojavanje.text = "";
    }

    IEnumerator cooldownPotions()
    {
        yield return new WaitForSeconds(30);
        btn_blue_potion.interactable = true;
        btn_orange_potion.interactable = true;
    }
}
