﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using System;

public class PlayerData : MonoBehaviour
{
    public TextMeshProUGUI lbl_player_name;
    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    //Ucitani podaci
    public static string username;
    public static int xp;
    public static int LVL;
    public static int c_health;
    public static int m_health;
    public static int e_c_health;
    public static int e_m_health;
    public static int BaseAttackDmg;
    public static int credits;
    public static int red_potions;
    public static int green_potions;
    public static int orange_potions;
    public static int blue_potions;
    public static int BonusAttack;
    public static int difficulty;

    public static bool lvl1;
    public static bool lvl2;
    public static bool lvl3;
    public static bool lvl4;
    public static bool lvl5;
    public static bool lvl6;
    public static bool lvl7;
    public static bool lvl8;

    public static bool rijeci10;
    public static bool rijeci25;
    public static bool rijeci50;
    public static bool rijeci100;
    public static bool rijeci250;
    public static bool rijeci500;
    public static bool rijeci1000;
    public static bool rijeci10_2;
    public static bool rijeci25_2;
    public static bool rijeci50_2;
    public static bool slova5;
    public static bool slova6;
    public static bool slova7;
    public static bool slova8;
    public static bool slova9;
    public static bool slova10;

    void Start()
    { 
        lbl_player_name.text = PlayerData.username;
    }

}
