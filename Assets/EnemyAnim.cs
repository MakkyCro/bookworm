﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAnim : MonoBehaviour
{
    public AudioClip s_punch;
    public AudioClip s_attack1;
    public AudioClip s_attack2;
    public AudioClip s_attack3;
    public AudioClip s_victory;

    public Animator animator;
    public Slider health;
    float time_hit;
    float time_attack;
    float time_death;

    public GameObject blood;

    public GameObject dragonAttack;

    public GameObject punchEffect1;
    public GameObject punchEffect2;

    public GameObject wizardAttack1;
    public GameObject wizardAttack2;
    public GameObject wizardAttack3;

    public GameObject entAttack1;
    public GameObject entAttack2;
    public GameObject entAttack3;
    void Start()
    {
        RuntimeAnimatorController ac = animator.runtimeAnimatorController;
        for (int i = 0; i < ac.animationClips.Length; i++)
        {
            if (ac.animationClips[i].name.Contains("Hit"))
            {
                time_hit = ac.animationClips[i].length;
            }
            if (ac.animationClips[i].name.Contains("Attack"))
            {
                time_attack = ac.animationClips[i].length;
            }
            if (ac.animationClips[i].name.Contains("Death"))
            {
                time_death = ac.animationClips[i].length;
            }
        }
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!animator.GetBool("Attack"))
        {
            System.Random r = new System.Random();
            int n = r.Next(1, 2);
            if (n == 1)
                Instantiate(punchEffect1, new Vector3(6f, 1.5f, 0), Quaternion.identity);
            else
                Instantiate(punchEffect2, new Vector3(6f, 1.5f, 0), Quaternion.identity);

            this.GetComponent<AudioSource>().clip = s_punch;
            this.GetComponent<AudioSource>().Play();
        }
 
        if (!animator.GetBool("Attack") && !animator.GetBool("Hit") && health.value > 0)
        {
            animator.SetBool("Hit", true);
            StartCoroutine(DisableHitAnimation());
            StartCoroutine(DisableAttackAnimation());
        }
        else if(!animator.GetBool("Attack") && !animator.GetBool("Hit") && health.value <= 0)
        {
            animator.SetBool("Death", true);
            StartCoroutine(DisableDeathAnimation());
            this.GetComponent<AudioSource>().clip = s_punch;
            this.GetComponent<AudioSource>().Play();
        }
    }

    IEnumerator DisableHitAnimation()
    {
        yield return new WaitForSeconds(time_hit);

        
        animator.SetBool("Hit", false);
        animator.SetBool("Attack", true);

        StartCoroutine(PlayCounterAttackAnimations());
    }

    IEnumerator PlayCounterAttackAnimations()
    {
        yield return new WaitForSeconds(1.6f);

        string enemy_name = transform.parent.name;

        if (enemy_name == "wizard")
        {
            System.Random rnd = new System.Random();
            int num = rnd.Next(1, 3);
            if (num == 1)
                { Instantiate(wizardAttack1, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack1; }
            else if (num == 2)
                { Instantiate(wizardAttack2, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack2; }
            else if (num == 3)
                {   Instantiate(wizardAttack3, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack3; }
        }
        else if (enemy_name == "Ent")
        {
            System.Random rnd = new System.Random();
            int num = rnd.Next(1, 3);
            if (num == 1)
            { Instantiate(entAttack1, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack1; }
            else if (num == 2)
            { Instantiate(entAttack2, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack2; }
            else if (num == 3)
            { Instantiate(entAttack3, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack3; }
        }
        else if (enemy_name == "dragon")
        {
            Instantiate(dragonAttack, new Vector3(-5.56f, 1.52f, 0), Quaternion.identity); this.GetComponent<AudioSource>().clip = s_attack1;
            StartCoroutine(DisableDragonFire());
        }
        else
        {
            Instantiate(blood, new Vector3(-5.2f, 1.5f, 0), Quaternion.identity);

            this.GetComponent<AudioSource>().clip = s_attack1;
        }
        this.GetComponent<AudioSource>().Play();
    }
    IEnumerator DisableDragonFire()
    {
        yield return new WaitForSeconds(.5f);
        Destroy(GameObject.Find("CFX4 Fire(Clone)"));
    }

    IEnumerator DisableAttackAnimation()
    {
        yield return new WaitForSeconds(time_attack + time_hit + 2f);
        animator.SetBool("Attack", false);
    }

    IEnumerator DisableDeathAnimation()
    {
        yield return new WaitForSeconds(time_death);
        animator.SetBool("Death", false);
        this.GetComponent<AudioSource>().clip = s_victory;
        this.GetComponent<AudioSource>().Play();
    }

}
