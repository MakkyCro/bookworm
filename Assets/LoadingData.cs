﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class LoadingData : MonoBehaviour
{
    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    public Text lbl_credits;

    public void LoadData()
    {
        PlayerData.username = MenuScript.usernameLogin;
        if (File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
        {
            var data = File.ReadAllLines(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt");
            var lista = new List<string>(data);
            PlayerData.username = lista[0];
            PlayerData.xp = int.Parse(lista[1]);
            PlayerData.LVL = int.Parse(lista[2]);
            PlayerData.c_health = int.Parse(lista[3]);
            PlayerData.m_health = int.Parse(lista[4]);
            PlayerData.e_c_health = int.Parse(lista[5]);
            PlayerData.e_m_health = int.Parse(lista[6]);
            PlayerData.BaseAttackDmg = int.Parse(lista[7]);
            PlayerData.credits = int.Parse(lista[8]);
            PlayerData.red_potions = int.Parse(lista[9]);
            PlayerData.green_potions = int.Parse(lista[10]);
            PlayerData.orange_potions = int.Parse(lista[11]);
            PlayerData.blue_potions = int.Parse(lista[12]);
            PlayerData.BonusAttack = int.Parse(lista[13]);
            PlayerData.difficulty = int.Parse(lista[14]);
            PlayerData.lvl1 = bool.Parse(lista[15]);
            PlayerData.lvl2 = bool.Parse(lista[16]);
            PlayerData.lvl3 = bool.Parse(lista[17]);
            PlayerData.lvl4 = bool.Parse(lista[18]);
            PlayerData.lvl5 = bool.Parse(lista[19]);
            PlayerData.lvl6 = bool.Parse(lista[20]);
            PlayerData.lvl7 = bool.Parse(lista[21]);
            PlayerData.lvl8 = bool.Parse(lista[22]);
            lbl_credits.text = PlayerData.credits.ToString();
        }
        else
        {
            PlayerData.username = MenuScript.usernameLogin;
            PlayerData.xp = 0;
            PlayerData.LVL = 1;
            PlayerData.c_health = 100;
            PlayerData.m_health = 100;
            PlayerData.e_c_health = 100;
            PlayerData.e_m_health = 100;
            PlayerData.BaseAttackDmg = 10;
            PlayerData.credits = 100;
            PlayerData.red_potions = 3;
            PlayerData.green_potions = 3;
            PlayerData.orange_potions = 2;
            PlayerData.blue_potions = 1;
            PlayerData.BonusAttack = 0;
            PlayerData.difficulty = 1;
            PlayerData.lvl1 = true;
            PlayerData.lvl2 = false;
            PlayerData.lvl3 = false;
            PlayerData.lvl4 = false;
            PlayerData.lvl5 = false;
            PlayerData.lvl6 = false;
            PlayerData.lvl7 = false;
            PlayerData.lvl8 = false;
            lbl_credits.text = PlayerData.credits.ToString();
            if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
            {
                using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt")) { }
            }
            File.AppendAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.xp + "\n" + PlayerData.LVL + "\n" + PlayerData.c_health + "\n" + PlayerData.m_health + "\n" + PlayerData.e_c_health + "\n" + PlayerData.e_m_health + "\n" +
                 PlayerData.BaseAttackDmg + "\n" + PlayerData.credits + "\n" + PlayerData.red_potions + "\n" + PlayerData.green_potions + "\n" + PlayerData.orange_potions + "\n" + PlayerData.blue_potions + "\n" + PlayerData.BonusAttack + "\n" + PlayerData.difficulty + "\n"
                 +PlayerData.lvl1 + "\n" + PlayerData.lvl2 + "\n" + PlayerData.lvl3 + "\n" + PlayerData.lvl4 + "\n" + PlayerData.lvl5 + "\n"
                  + PlayerData.lvl6 + "\n" + PlayerData.lvl7 + "\n" + PlayerData.lvl8);
        }

        if (File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt"))
        {
            var data = File.ReadAllLines(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt");
            var lista = new List<string>(data);
            PlayerData.rijeci10 = bool.Parse(lista[0]);
            PlayerData.rijeci25 = bool.Parse(lista[1]);
            PlayerData.rijeci50 = bool.Parse(lista[2]);
            PlayerData.rijeci100 = bool.Parse(lista[3]);
            PlayerData.rijeci250 = bool.Parse(lista[4]);
            PlayerData.rijeci500 = bool.Parse(lista[5]);
            PlayerData.rijeci100 = bool.Parse(lista[6]);
            PlayerData.rijeci10_2 = bool.Parse(lista[7]);
            PlayerData.rijeci25_2 = bool.Parse(lista[8]);
            PlayerData.rijeci50_2 = bool.Parse(lista[9]);
            PlayerData.slova5 = bool.Parse(lista[10]);
            PlayerData.slova6 = bool.Parse(lista[11]);
            PlayerData.slova7 = bool.Parse(lista[12]);
            PlayerData.slova8 = bool.Parse(lista[13]);
            PlayerData.slova9 = bool.Parse(lista[14]);
            PlayerData.slova10 = bool.Parse(lista[15]);
        }
        else
        {
            PlayerData.rijeci10 = false;
            PlayerData.rijeci25 = false;
            PlayerData.rijeci50 = false;
            PlayerData.rijeci100 = false;
            PlayerData.rijeci250 = false;
            PlayerData.rijeci500 = false;
            PlayerData.rijeci1000 = false;
            PlayerData.rijeci10_2 = false;
            PlayerData.rijeci25_2 = false;
            PlayerData.rijeci50_2 = false;
            PlayerData.slova5 = false;
            PlayerData.slova6 = false;
            PlayerData.slova7 = false;
            PlayerData.slova8 = false;
            PlayerData.slova9 = false;
            PlayerData.slova10 = false;

            if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt"))
            {
                using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt")) { }
            }
            File.AppendAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt", 
                         PlayerData.rijeci10
                + "\n" + PlayerData.rijeci25
                + "\n" + PlayerData.rijeci50
                + "\n" + PlayerData.rijeci100
                + "\n" + PlayerData.rijeci250
                + "\n" + PlayerData.rijeci500
                + "\n" + PlayerData.rijeci1000
                + "\n" + PlayerData.rijeci10_2
                + "\n" + PlayerData.rijeci25_2
                + "\n" + PlayerData.rijeci50_2
                + "\n" + PlayerData.slova5
                + "\n" + PlayerData.slova6
                + "\n" + PlayerData.slova7
                + "\n" + PlayerData.slova8
                + "\n" + PlayerData.slova9
                + "\n" + PlayerData.slova10);
        }
    }

}
