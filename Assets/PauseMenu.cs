﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    public AudioClip sound_button;
    public AudioClip sound_pause;

    public GameObject pauseMenuCanvas;
    public GameObject gameOverScreen;
    public GameObject panel_trgovina;
    public GameObject panel_PauseMeni;

    public TextMeshProUGUI lbl_red;
    public TextMeshProUGUI lbl_green;
    public TextMeshProUGUI lbl_orange;
    public TextMeshProUGUI lbl_blue;

    public Game_Managment gm;
    public Game_Managment_TimeAttack gmTimeAttack;

    public TextMeshProUGUI lbl_money;
    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    void Start()
    {
        try
        {
            gm = GameObject.FindObjectOfType(typeof(Game_Managment)) as Game_Managment;
        }
        catch { }
        try
        {
            gmTimeAttack = GameObject.FindObjectOfType(typeof(Game_Managment_TimeAttack)) as Game_Managment_TimeAttack;
        }
        catch { }
        
        lbl_red.text = "x" + PlayerData.red_potions;
        lbl_green.text = "x" + PlayerData.green_potions;
        lbl_orange.text = "x" + PlayerData.orange_potions;
        lbl_blue.text = "x" + PlayerData.blue_potions;
    }

    void Update()
    {
        if (pauseMenuCanvas.activeSelf == false && Input.GetKeyDown("escape") && gameOverScreen.activeSelf == false)
        {
            this.GetComponent<AudioSource>().clip = sound_pause;
            this.GetComponent<AudioSource>().Play();
            pauseMenuCanvas.SetActive(true);
            lbl_money.text = PlayerData.credits.ToString();
            Time.timeScale = 0f;
        }
        else if (pauseMenuCanvas.activeSelf == true && Input.GetKeyDown("escape") && gameOverScreen.activeSelf == false)
        {
            Time.timeScale = 1f;
            try
            {
                gm.refreshStats();
            }
            catch { }
            try
            {
                gmTimeAttack.refreshStats();
            }
            catch { }
            pauseMenuCanvas.SetActive(false);
        }
    }

    public void ResumePressed()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        Time.timeScale = 1f;
        try
        {
            gm.refreshStats();
        }
        catch { }
        try
        {
            gmTimeAttack.refreshStats();
        }
        catch { }
        pauseMenuCanvas.SetActive(false);
    }
    public void NazadPressed()
    {

        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        panel_trgovina.SetActive(false);
        panel_PauseMeni.SetActive(true);
    }
    public void TrgovinaPressed()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        RefreshData();
        panel_PauseMeni.SetActive(false);
        panel_trgovina.SetActive(true);
    }
    public void MainMenuPressed()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        Time.timeScale = 1f;
        AutoFade.LoadLevel("MainMenu", 1, 1, Color.black);
    }
    void RefreshData()
    {
        LoadData();
        lbl_money.text = PlayerData.credits.ToString();

        lbl_red.text = "x" + PlayerData.red_potions;
        lbl_green.text = "x" + PlayerData.green_potions;
        lbl_orange.text = "x" + PlayerData.orange_potions;
        lbl_blue.text = "x" + PlayerData.blue_potions;
    }
    public void LoadData()
    {
        PlayerData.username = MenuScript.usernameLogin;
        if (File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
        {
            var usernames = File.ReadAllLines(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt");
            var lista = new List<string>(usernames);
            PlayerData.username = lista[0];
            PlayerData.xp = int.Parse(lista[1]);
            PlayerData.LVL = int.Parse(lista[2]);
            PlayerData.c_health = int.Parse(lista[3]);
            PlayerData.m_health = int.Parse(lista[4]);
            PlayerData.e_c_health = int.Parse(lista[5]);
            PlayerData.e_m_health = int.Parse(lista[6]);
            PlayerData.BaseAttackDmg = int.Parse(lista[7]);
            PlayerData.credits = int.Parse(lista[8]);
            PlayerData.red_potions = int.Parse(lista[9]);
            PlayerData.green_potions = int.Parse(lista[10]);
            PlayerData.orange_potions = int.Parse(lista[11]);
            PlayerData.blue_potions = int.Parse(lista[12]);
            PlayerData.BonusAttack = int.Parse(lista[13]);
            PlayerData.difficulty = int.Parse(lista[14]);
            PlayerData.lvl1 = bool.Parse(lista[15]);
            PlayerData.lvl2 = bool.Parse(lista[16]);
            PlayerData.lvl3 = bool.Parse(lista[17]);
            PlayerData.lvl4 = bool.Parse(lista[18]);
            PlayerData.lvl5 = bool.Parse(lista[19]);
            PlayerData.lvl6 = bool.Parse(lista[20]);
            PlayerData.lvl7 = bool.Parse(lista[21]);
            PlayerData.lvl8 = bool.Parse(lista[22]);
            lbl_money.text = PlayerData.credits.ToString();
        }
        else
        {
            PlayerData.username = MenuScript.usernameLogin;
            PlayerData.xp = 0;
            PlayerData.LVL = 1;
            PlayerData.c_health = 100;
            PlayerData.m_health = 100;
            PlayerData.e_c_health = 100;
            PlayerData.e_m_health = 100;
            PlayerData.BaseAttackDmg = 30;
            PlayerData.credits = 100;
            PlayerData.red_potions = 5;
            PlayerData.green_potions = 5;
            PlayerData.orange_potions = 3;
            PlayerData.blue_potions = 3;
            PlayerData.BonusAttack = 0;
            PlayerData.difficulty = 1;
            PlayerData.lvl1 = true;
            PlayerData.lvl2 = false;
            PlayerData.lvl3 = false;
            PlayerData.lvl4 = false;
            PlayerData.lvl5 = false;
            PlayerData.lvl6 = false;
            PlayerData.lvl7 = false;
            PlayerData.lvl8 = false;
            lbl_money.text = PlayerData.credits.ToString();
            if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
            {
                using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt")) { }
            }
            File.AppendAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.xp + "\n" + PlayerData.LVL + "\n" + PlayerData.c_health + "\n" + PlayerData.m_health + "\n" + PlayerData.e_c_health + "\n" + PlayerData.e_m_health + "\n" +
      PlayerData.BaseAttackDmg + "\n" + PlayerData.credits + "\n" + PlayerData.red_potions + "\n" + PlayerData.green_potions + "\n" + PlayerData.orange_potions + "\n" + PlayerData.blue_potions + "\n" + PlayerData.BonusAttack + "\n" + PlayerData.difficulty + "\n"
      + PlayerData.lvl1 + "\n" + PlayerData.lvl2 + "\n" + PlayerData.lvl3 + "\n" + PlayerData.lvl4 + "\n" + PlayerData.lvl5 + "\n"
       + PlayerData.lvl6 + "\n" + PlayerData.lvl7 + "\n" + PlayerData.lvl8);
        }
    }

    void saveChange()
    {
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.xp + "\n" + PlayerData.LVL + "\n" + PlayerData.c_health + "\n" + PlayerData.m_health + "\n" + PlayerData.e_c_health + "\n" + PlayerData.e_m_health + "\n" +
                PlayerData.BaseAttackDmg + "\n" + PlayerData.credits + "\n" + PlayerData.red_potions + "\n" + PlayerData.green_potions + "\n" + PlayerData.orange_potions + "\n" + PlayerData.blue_potions + "\n" + PlayerData.BonusAttack + "\n" + PlayerData.difficulty + "\n" + PlayerData.lvl1 + "\n" + PlayerData.lvl2 + "\n" + PlayerData.lvl3 + "\n" + PlayerData.lvl4 + "\n" + PlayerData.lvl5 + "\n"
                 + PlayerData.lvl6 + "\n" + PlayerData.lvl7 + "\n" + PlayerData.lvl8 + "\n");
        File.WriteAllText(pathDocuments + "\\Bookworm\\Postignuca_" + PlayerData.username + ".txt",
                    PlayerData.rijeci10
           + "\n" + PlayerData.rijeci25
           + "\n" + PlayerData.rijeci50
           + "\n" + PlayerData.rijeci100
           + "\n" + PlayerData.rijeci250
           + "\n" + PlayerData.rijeci500
           + "\n" + PlayerData.rijeci1000
           + "\n" + PlayerData.rijeci10_2
           + "\n" + PlayerData.rijeci25_2
           + "\n" + PlayerData.rijeci50_2
           + "\n" + PlayerData.slova5
           + "\n" + PlayerData.slova6
           + "\n" + PlayerData.slova7
           + "\n" + PlayerData.slova8
           + "\n" + PlayerData.slova9
           + "\n" + PlayerData.slova10);
    }

    public void BuyRedPotion()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 50)
        {
            PlayerData.red_potions += 1;
            PlayerData.credits -= 50;
            saveChange();
            RefreshData();
        }
    }
    public void BuyGreenPotion()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 75)
        {
            PlayerData.green_potions += 1;
            PlayerData.credits -= 75;
            saveChange();
            RefreshData();
        }
    }
    public void BuyOrangePotion()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 100)
        {
            PlayerData.orange_potions += 1;
            PlayerData.credits -= 100;
            saveChange();
            RefreshData();
        }
    }
    public void BuyBluePotion()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        if (PlayerData.credits >= 125)
        {
            PlayerData.blue_potions += 1;
            PlayerData.credits -= 125;
            saveChange();
            RefreshData();
        }
    }
    public void izlazPressed()
    {
        this.GetComponent<AudioSource>().clip = sound_button;
        this.GetComponent<AudioSource>().Play();
        Time.timeScale = 1f;
        if (!Application.isEditor) System.Diagnostics.Process.GetCurrentProcess().Kill();
    }
}
