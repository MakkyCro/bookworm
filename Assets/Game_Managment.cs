﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class Game_Managment : MonoBehaviour
{
    public GridLayoutGroup layoutGeneratedLetters;
    public GridLayoutGroup layoutPressedLetters;

    public Button button;
    public Button btn_attack;
    public Button btn_red_potion;
    public Button btn_green_potion;

    public TextMeshProUGUI lbl_e_health;
    public TextMeshProUGUI lbl_money;
    public TextMeshProUGUI lbl_xp;
    public TextMeshProUGUI lbl_xp_result;
    public TextMeshProUGUI lbl_player_name;
    public TextMeshProUGUI lbl_lvl_up_xp_canvas;

    public Text lbl_num_red_potions;
    public Text lbl_num_green_potions;
    public Text tb_imenice;
    public Text tb_imenice_enemy;
    public Text lbl_stats;
    public Text lbl_name_result;
    public Text lbl_new_words_result;
    public Text lbl_lvl_up_canvas;

    public Slider slider_p_health;
    public Slider slider_e_health;
    public Slider slider_xp;
    public Slider slider_xp_result;
    public Slider slider_xp_LVLup;

    public GameObject resultCanvas;
    public GameObject gameOverCanvas;
    public GameObject victoryCanvas;
    public GameObject lvlUpCanvas;
    public GameObject bg;
    public GameObject enemy_skull;
    public GameObject enemy_ninja;
    public GameObject enemy_wizard;
    public GameObject enemy_ent;
    public GameObject enemy_orc;
    public GameObject enemy_troll;
    public GameObject enemy_golem;
    public GameObject enemy_dragon;
    public GameObject player_boxingGloves;
    public GameObject HeartAnimation;
    public GameObject AuraAnimation;

    public AudioClip sound_add;
    public AudioClip sound_potion;
    public AudioClip sound_izmjesaj;
    public AudioClip sound_resetiraj;

    public Sprite bg1;
    public Sprite bg2;
    public Sprite bg3;
    public Sprite bg4;
    public Sprite bg5;
    public Sprite bg6;
    public Sprite bg7;
    public Sprite bg8;

    public Animator animator_player;

    public GeneratorSlova gs;

    char[] samoglasnici = { 'A', 'E', 'I', 'O', 'U' };
    char[] ostala_slova = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S',  'T', 'V', 'Z' };
    char[] rijetka_slova = { 'Č', 'Ć',  'Đ', 'Š', 'Ž' };

    public static List<char> slova = new List<char>();
    public static string word;
    public static string[] imenice;
    public static List<string> spremljene_rijeci = new List<string>();
    public static List<string> rijeci_igraca = new List<string>();

    string pathDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    int[] LVL_borders = { 50, 125, 300, 500, 750, 1000, 1500 };
    string[] naziv_neprijatelja = { "Kostur", "Ninja", "Čarobnjak", "Stablo" ,"Ork" ,"Trol" ,"Kameni","Zmaj" };

    int baseDmgByLVL;
    int bonusAttack = 0;
    bool drink = false;
    int lvl = 0;
    void Start()
    {
        rijeci_igraca.Clear();
        LoadBackground();
        LoadEnemy();
        SetHealth();

        gs = GameObject.FindObjectOfType(typeof(GeneratorSlova)) as GeneratorSlova;
        CalculateBaseAttack();
        lbl_name_result.text = PlayerData.username;
        btn_attack.interactable = false;
        imenice = File.ReadAllLines(pathDocuments + "\\Bookworm\\imeniceBookWorm.txt");
        gs.GenerirajSlova();
        refreshStats();
        lvl = CalculateLVL(PlayerData.xp);
    }

    void SetHealth()
    {
        PlayerData.e_m_health = 50 + 50 * MenuScript.lvl;
        PlayerData.e_c_health = PlayerData.e_m_health;
        PlayerData.m_health = 50 + 50 * MenuScript.lvl + (CalculateLVL(PlayerData.xp)*5);
        PlayerData.c_health = PlayerData.m_health;
    }
    void LoadBackground()
    {
        if (MenuScript.lvl == 1)
            bg.GetComponent<Image>().sprite = bg1;
        else if (MenuScript.lvl == 2)
            bg.GetComponent<Image>().sprite = bg2;
        else if (MenuScript.lvl == 3)
            bg.GetComponent<Image>().sprite = bg3;
        else if (MenuScript.lvl == 4)
            bg.GetComponent<Image>().sprite = bg4;
        else if (MenuScript.lvl == 5)
            bg.GetComponent<Image>().sprite = bg5;
        else if (MenuScript.lvl == 6)
            bg.GetComponent<Image>().sprite = bg6;
        else if (MenuScript.lvl == 7)
            bg.GetComponent<Image>().sprite = bg7;
        else if (MenuScript.lvl == 8)
            bg.GetComponent<Image>().sprite = bg8;
    }
    void LoadEnemy()
    {
        enemy_skull.SetActive(false);
        enemy_ninja.SetActive(false);
        enemy_wizard.SetActive(false);
        enemy_ent.SetActive(false);
        enemy_orc.SetActive(false);
        enemy_troll.SetActive(false);
        enemy_golem.SetActive(false);
        enemy_dragon.SetActive(false);

        if (MenuScript.lvl == 1)
            enemy_skull.SetActive(true);
        else if (MenuScript.lvl == 2)
            enemy_ninja.SetActive(true);
        else if (MenuScript.lvl == 3)
            enemy_wizard.SetActive(true);
        else if (MenuScript.lvl == 4)
            enemy_ent.SetActive(true);
        else if (MenuScript.lvl == 5)
            enemy_orc.SetActive(true);
        else if (MenuScript.lvl == 6)
            enemy_troll.SetActive(true);
        else if (MenuScript.lvl == 7)
            enemy_golem.SetActive(true);
        else if (MenuScript.lvl == 8)
            enemy_dragon.SetActive(true);
    }
    public void SaveProgress()
    {
        if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt")) { }
        }
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + ".txt", PlayerData.username + "\n" + PlayerData.xp + "\n" + PlayerData.LVL + "\n" + PlayerData.c_health + "\n" + PlayerData.m_health + "\n" + PlayerData.e_c_health + "\n" + PlayerData.e_m_health + "\n" +
                PlayerData.BaseAttackDmg + "\n" + PlayerData.credits + "\n" + PlayerData.red_potions + "\n" + PlayerData.green_potions + "\n" + PlayerData.orange_potions + "\n" + PlayerData.blue_potions + "\n" + PlayerData.BonusAttack + "\n" + PlayerData.difficulty + "\n" + PlayerData.lvl1 + "\n" + PlayerData.lvl2 + "\n" + PlayerData.lvl3 + "\n" + PlayerData.lvl4 + "\n" + PlayerData.lvl5 + "\n"
                 + PlayerData.lvl6 + "\n" + PlayerData.lvl7 + "\n" + PlayerData.lvl8 + "\n");

        if (!File.Exists(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt")) { }
        }
        File.WriteAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_postignuca.txt",
                      PlayerData.rijeci10
             + "\n" + PlayerData.rijeci25
             + "\n" + PlayerData.rijeci50
             + "\n" + PlayerData.rijeci100
             + "\n" + PlayerData.rijeci250
             + "\n" + PlayerData.rijeci500
             + "\n" + PlayerData.rijeci1000
             + "\n" + PlayerData.rijeci10_2
             + "\n" + PlayerData.rijeci25_2
             + "\n" + PlayerData.rijeci50_2
             + "\n" + PlayerData.slova5
             + "\n" + PlayerData.slova6
             + "\n" + PlayerData.slova7
             + "\n" + PlayerData.slova8
             + "\n" + PlayerData.slova9
             + "\n" + PlayerData.slova10);
    }
    
    public void UseRedPotion()
    {
        if(PlayerData.red_potions > 0 && slider_p_health.value < slider_p_health.maxValue)
        {
            this.GetComponent<AudioSource>().clip = sound_potion;
            this.GetComponent<AudioSource>().Play();

            PlayerData.c_health = PlayerData.m_health;
            PlayerData.red_potions -= 1;
            btn_red_potion.interactable = false;
            Instantiate(HeartAnimation, new Vector3(-7.6f, 0.0f, 0), Quaternion.identity);
        }
        refreshStats();
        SaveProgress();
    }

    public void UseGreenPotion()
    {
        if (PlayerData.green_potions > 0)
        {

            this.GetComponent<AudioSource>().clip = sound_potion;
            this.GetComponent<AudioSource>().Play();

            drink = true;
            CalculateBaseAttack();
            btn_green_potion.interactable = false;
            PlayerData.BaseAttackDmg += 10;
            PlayerData.green_potions -= 1;
            Instantiate(AuraAnimation, new Vector3(-5.36f, -0.8f, 0), Quaternion.identity);
        }
        refreshStats();
        SaveProgress();
    }

    public void refreshStats()
    {
        lbl_e_health.text = PlayerData.e_c_health.ToString();
        slider_p_health.maxValue = PlayerData.m_health;
        slider_p_health.value = PlayerData.c_health;
        slider_e_health.maxValue = PlayerData.e_m_health;
        slider_e_health.value = PlayerData.e_c_health;       

        lbl_num_red_potions.text = "x "+PlayerData.red_potions.ToString();
        lbl_num_green_potions.text = "x "+PlayerData.green_potions.ToString();

        lbl_xp.text = PlayerData.xp + " xp / "+CalculateXP(PlayerData.xp) + " xp ("+CalculateLVL(PlayerData.xp)+ " LVL)";
        slider_xp.maxValue = CalculateXP(PlayerData.xp);
        slider_xp.value = PlayerData.xp;

        lbl_xp_result.text = PlayerData.xp + " xp / " + CalculateXP(PlayerData.xp) + " xp (" + CalculateLVL(PlayerData.xp) + " LVL)";
        slider_xp_result.maxValue = CalculateXP(PlayerData.xp);
        slider_xp_result.value = PlayerData.xp;

        lbl_money.text = PlayerData.credits.ToString();
        lbl_stats.text = "Naziv neprijatelja: " + naziv_neprijatelja[MenuScript.lvl-1] + "\n\nTvoj temeljni napad: " + PlayerData.BaseAttackDmg + "\nPotencijalni dodatani napad: " + gs.bonusAttack + "\nDodatan napad od zadnje riječi: " + bonusAttack + "\nProsječan broj iskorištenih slova po riječi: " + ProsjekSlova().ToString("F2");

        int healthPercent = (int)Math.Round(slider_p_health.value / slider_p_health.maxValue * 100f);
        lbl_player_name.text = PlayerData.username + " - " + healthPercent + "%";

        lbl_lvl_up_canvas.text = CalculateLVL(PlayerData.xp).ToString();
        lbl_lvl_up_xp_canvas.text = PlayerData.xp + " xp / " + CalculateXP(PlayerData.xp) + " xp";
        slider_xp_LVLup.maxValue = CalculateXP(PlayerData.xp);
        slider_xp_LVLup.value = PlayerData.xp;
    }

    public int CalculateBaseAttack()
    {
        baseDmgByLVL = CalculateLVL(PlayerData.xp) * 10;
        PlayerData.BaseAttackDmg = baseDmgByLVL + Convert.ToInt32(PlayerData.xp / 10);
        return PlayerData.BaseAttackDmg;
    }

    public float ProsjekSlova()
    {
        float prosjek = 0.0f;

        for(int i = 0; i < rijeci_igraca.Count; i++)
        {
            prosjek += rijeci_igraca[i].Length;
        }
        if(prosjek == 0.0f)
        {
            return 0.0f;
        }
        prosjek = prosjek / (float)rijeci_igraca.Count;

        return prosjek;
    }

    public int CalculateLVL(int xp)
    {
        int LVL = 1;
        foreach(int max_xp in LVL_borders)
        {
            if(xp >= max_xp)
            {
                LVL++;
            }
            else { break; }
        }
        PlayerData.LVL = LVL;
        return LVL;
    }

    public int CalculateXP(int xp)
    {
        int xp_nextLVL = 100;
        foreach (int max_xp in LVL_borders)
        {
            if (xp >= max_xp)
            {
                xp_nextLVL = max_xp;
            }
            else {
                xp_nextLVL = max_xp;
                break; }
        }
        return xp_nextLVL;
    }
    public void Attack()
    {
        lvl = CalculateLVL(PlayerData.xp);

        player_boxingGloves.SetActive(true);
        animator_player.SetBool("Attack", true);
        StartCoroutine(WaitAttackAnimPlayer());
        btn_attack.interactable = false;
        Obrisi(true);
        gs.brojac = 0;

        string imenica = "";
        bonusAttack = 0;

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("Copy") && !btn1.name.Contains("Green") && !btn1.name.Contains("Red"))
            {
                bonusAttack += 1;
                imenica += btn1.name.Split('_')[2];
            }
            else if(btn1.name.Contains("Copy") && btn1.name.Contains("Green"))
            {
                bonusAttack += 2;
                imenica += btn1.name.Split('_')[2];
            }
            else if(btn1.name.Contains("Copy") && btn1.name.Contains("Red"))
            {
                bonusAttack += 5;
                imenica += btn1.name.Split('_')[2];
            }
        }

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("BTN") || btn1.name.Contains("Bttn"))
            {
                btn1.interactable = false;
            }
        }

        imenica = Reverse(imenica);
        spremljene_rijeci.Add(imenica);
        rijeci_igraca.Add(imenica);
        tb_imenice.text += imenica + " ";
        OduzmiHPiDodajXPiKredite(bonusAttack);
        SpremiRijec(imenica);
        DodajNovuRijecResult(imenica);

        if (PlayerData.e_c_health <= 0)
        {
            UnlockNextLVL();
            PlayerData.e_c_health = 0;
            MenuScript.lvl += 1;
            StartCoroutine(WaitDeathAnimation());
        }
        else
        {
            StartCoroutine(NapadNeprijatelja());
            if (btn_green_potion.interactable == false && drink)
            {
                //btn_green_potion.interactable = true;
                PlayerData.BaseAttackDmg -= 10;
                StartCoroutine(AuraAnimationOffset());
                drink = false;
            }
            if (btn_red_potion.interactable == false)
            {
                //btn_red_potion.interactable = true;
            }
        }
        CheckForAchivement(imenica);
        gs.bonusAttack = 0;
        refreshStats();
        SaveProgress();
    }
    
    void CheckForLVLup(int lvl)
    {
        int tmp = CalculateLVL(PlayerData.xp);
        if(tmp != lvl)
        {
            Time.timeScale = 0f;
            lvlUpCanvas.SetActive(true);
        }
    }
    void CheckForAchivement( string rijec)
    {
        var data = File.ReadAllLines(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt");
        var lista = new List<string>(data);

        if(lista.Count == 10 && !PlayerData.rijeci10)
            PlayerData.rijeci10 = true;
        if (lista.Count == 25 && !PlayerData.rijeci25)
            PlayerData.rijeci25 = true;
        if (lista.Count == 50 && !PlayerData.rijeci50)
            PlayerData.rijeci50 = true;
        if (lista.Count == 100 && !PlayerData.rijeci100)
            PlayerData.rijeci100 = true;
        if (lista.Count == 250 && !PlayerData.rijeci250)
            PlayerData.rijeci250 = true;
        if (lista.Count == 500 && !PlayerData.rijeci500)
            PlayerData.rijeci500 = true;
        if (lista.Count == 1000 && !PlayerData.rijeci1000)
            PlayerData.rijeci1000 = true;

        if (rijec.Length == 5 && !PlayerData.slova5)
            PlayerData.slova5 = true;
        if (rijec.Length == 6 && !PlayerData.slova6)
            PlayerData.slova6 = true;
        if (rijec.Length == 7 && !PlayerData.slova7)
            PlayerData.slova7 = true;
        if (rijec.Length == 8 && !PlayerData.slova8)
            PlayerData.slova8 = true;
        if (rijec.Length == 9 && !PlayerData.slova9)
            PlayerData.slova9 = true;
        if (rijec.Length >= 10 && !PlayerData.slova10)
            PlayerData.slova10 = true;
    }

    IEnumerator AuraAnimationOffset()
    {
        yield return new WaitForSeconds(2f);
        Destroy(GameObject.Find("CFX4 Aura Bubble C(Clone)"));
    }
    IEnumerator WaitDeathAnimation()
    {
        yield return new WaitForSeconds(5f);
        CheckForLVLup(lvl);
        if (MenuScript.lvl-1 != 8)
            resultCanvas.SetActive(true);
        else
            victoryCanvas.SetActive(true);
    }

    void UnlockNextLVL()
    {
        if (MenuScript.lvl == 1)
            PlayerData.lvl2 = true;
        else if (MenuScript.lvl == 2)
            PlayerData.lvl3 = true;
        else if (MenuScript.lvl == 3)
            PlayerData.lvl4 = true;
        else if (MenuScript.lvl == 4)
            PlayerData.lvl5 = true;
        else if (MenuScript.lvl == 5)
            PlayerData.lvl6 = true;
        else if (MenuScript.lvl == 6)
            PlayerData.lvl7 = true;
        else if (MenuScript.lvl == 7)
            PlayerData.lvl8 = true;
    }
    
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
    public void DodajNovuRijecResult(string unos)
    {
        bool postoji = false;
        //string[] rijeci = File.ReadAllLines(pathDocuments+ "\\Bookworm\\imeniceBookWorm.txt");
        foreach(string r in imenice)
        {
            if(r == unos)
            {
                postoji = true;
                break;
            }
        }
        if(!postoji)
        {
            lbl_new_words_result.text += unos + "\t";
        }
    }

    public void SpremiRijec(string rijec)
    {
        if (!File.Exists(pathDocuments + "\\Bookworm\\"+PlayerData.username+"_rijeci.txt"))
        {
            using (File.Create(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt")) { }
        }
        File.AppendAllText(pathDocuments + "\\Bookworm\\" + PlayerData.username + "_rijeci.txt", rijec+"\n");
    }

    public void OduzmiHPiDodajXPiKredite(int bonusAttack)
    {
        PlayerData.e_c_health -= PlayerData.BaseAttackDmg + bonusAttack;
        PlayerData.xp += bonusAttack;
        PlayerData.credits += bonusAttack;
        refreshStats();
    }

    public void OduzmiHPiDodajXP_enemyAttack(int bonusAttack)
    {
        PlayerData.c_health -= (10* MenuScript.lvl) + bonusAttack;
        if(PlayerData.c_health <= 0)
        {
            Time.timeScale = 0f;
            gameOverCanvas.SetActive(true);
        }
        refreshStats();
        SaveProgress();
    }

    IEnumerator NapadNeprijatelja()
    {
        yield return new WaitForSeconds(2f);

        CheckForLVLup(lvl);

        char[] slovaTemp = slova.ToArray();

        List<string> lista = new List<string>();
        foreach(string imenica in imenice)
        {
            if(imenica.Length >=4 && imenica.Length <= (CalculateLVL(PlayerData.xp)+4))
            { lista.Add(imenica); }
        }

        System.Random rng = new System.Random();
        int n = lista.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            string value = lista[k];
            lista[k] = lista[n];
            lista[n] = value;
        }
        string rijec = "";
        bool pronadeno_slovo = false;
        foreach (string ime1 in lista)
        {
            string ime = ime1.ToUpper();
            if(!tb_imenice.text.Contains(ime + " ") && !tb_imenice_enemy.text.Contains(ime + " "))
            {
                string temp = "";
                for (int i = 0; i < ime.Length; i++)
                {
                    for (int j = 0; j < slovaTemp.Length; j++)
                    {
                        if (slovaTemp[j] == ime[i])
                        {
                            temp += slovaTemp[j];
                            slovaTemp[j] = ' ';
                            pronadeno_slovo = true;
                            break;
                        }
                        else { pronadeno_slovo = false; }
                    }
                    if(!pronadeno_slovo)
                    {
                        slovaTemp = slova.ToArray();
                    }
                }
                if(ime == temp)
                { 
                    spremljene_rijeci.Add(ime);
                    rijec = ime;
                    tb_imenice_enemy.text += ime + " ";
                    break;
                }
            }
        }
       
        List<string> iskoristeni = new List<string>();

        StartCoroutine(WaitHalfSec(iskoristeni, rijec));
        
        StartCoroutine(Wait2Sec());
    }

    public void Obrisi(bool temp = false)
    {
        if(!temp)
        {
            this.GetComponent<AudioSource>().clip = sound_resetiraj;
            this.GetComponent<AudioSource>().Play();
        }

        try
        {
            gs.bonusAttack = 0;
            gs.brojac = 0;
            foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn1.name.Contains("Copy"))
                {
                    Destroy(btn1);
                    Destroy(btn1.gameObject);
                }
                else
                {
                    if(btn1.name != "Bttn_Attack" && !btn1.name.Contains("redPotion") && !btn1.name.Contains("greenPotion"))
                    { btn1.interactable = true; }
                    else if (!btn1.name.Contains("redPotion") && !btn1.name.Contains("greenPotion"))
                    { btn1.interactable = false; }
                }
            }
            refreshStats();
        }
        catch { Debug.Log("Već je resetirano!"); }
    }
    public void Izmjesaj()
    {
        Obrisi(true);
        gs.brojac = 0;
        gs.bonusAttack = 0;

        this.GetComponent<AudioSource>().clip = sound_izmjesaj;
        this.GetComponent<AudioSource>().Play();

        List<string> generirana_slova = new List<string>();
        int i = 0;

        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (!btn1.name.Contains("Copy") && !btn1.name.Contains("Attack") && !btn1.name.Contains("Randomize") && !btn1.name.Contains("Clear") && btn1.name.Contains("BTN_"))
            {
                generirana_slova.Add(btn1.name.Split('_')[0] + "_" + btn1.name.Split('_')[1]);
                Destroy(btn1);
                Destroy(btn1.gameObject);
            }
        }

        for (var j = generirana_slova.Count -1; j > 0; j--)
        {
            var r = UnityEngine.Random.Range(0, j);
            var tmp = generirana_slova[j];
            generirana_slova[j] = generirana_slova[r];
            generirana_slova[r] = tmp;
        }

        foreach (string slovo in generirana_slova)
        {
            //Debug.Log(slovo);
            Button newButton = Instantiate(button) as Button;
            newButton.name = "BTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            if (slovo.Contains("Green"))
            {
                newButton.GetComponentInChildren<Text>().color = Color.green;
                newButton.name = "GreenBTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            else if (slovo.Contains("Red"))
            {
                newButton.GetComponentInChildren<Text>().color = Color.red;
                newButton.name = "RedBTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            else
            {
                newButton.GetComponentInChildren<Text>().color = Color.black;
                newButton.name = "BTN_" + generirana_slova[i].Split('_')[1] + "_" + i;
            }
            newButton.GetComponentInChildren<Text>().text = generirana_slova[i].Split('_')[1];
            newButton.GetComponent<Button>().onClick.AddListener(delegate { gs.button_Click(newButton); });
            newButton.transform.SetParent(layoutGeneratedLetters.transform, false);
            i++;
        }
    }

    IEnumerator WaitHalfSec(List<string> iskoristeni, string rijec)
    {
        int bonus_attack = 0;
        for (int i = 0; i < rijec.Length; i++)
        {
            foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
            {
                if (btn1.name.Contains("BTN") && btn1.name.Split('_')[1] == rijec[i].ToString())
                {
                    float r = UnityEngine.Random.Range(0.2f, 0.4f);
                    yield return new WaitForSeconds(r);
                    Button newButton = Instantiate(button) as Button;
                    newButton.GetComponentInChildren<Text>().text = btn1.GetComponentInChildren<Text>().text;
                    newButton.name = "Copy_" + btn1.name;

                    if (btn1.name.Contains("Green"))
                    {
                        newButton.GetComponentInChildren<Text>().color = Color.green;
                        bonus_attack += 2;
                    }
                    else if (btn1.name.Contains("Red"))
                    {
                        newButton.GetComponentInChildren<Text>().color = Color.red;
                        bonus_attack += 5;
                    }
                    else
                    {
                        newButton.GetComponentInChildren<Text>().color = Color.black;
                        bonus_attack += 1;
                    }
                    iskoristeni.Add(btn1.name.Split('_')[2]);
                    newButton.transform.SetParent(layoutPressedLetters.transform, false);
                    newButton.interactable = false;
                    break;
                }
            }
        }
        StartCoroutine(PlayerHealthBar(bonus_attack));
    }

    IEnumerator PlayerHealthBar(int bonus_attack)
    {
        yield return new WaitForSeconds(1f);
        OduzmiHPiDodajXP_enemyAttack(bonus_attack);
    }
    IEnumerator Wait2Sec()
    {
        yield return new WaitForSeconds(4);
        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("Copy"))
            {
                Destroy(btn1);
                Destroy(btn1.gameObject);
            }
        }
        foreach (Button btn1 in UnityEngine.Object.FindObjectsOfType(typeof(Button)))
        {
            if (btn1.name.Contains("BTN") || (btn1.name.Contains("Bttn") && !btn1.name.Contains("Attack")))
            {
                btn1.interactable = true;
            }
        }
    }
    IEnumerator WaitAttackAnimPlayer()
    {
        yield return new WaitForSeconds(1);
        animator_player.SetBool("Attack", false);
        animator_player.SetBool("Attack2", false);
        player_boxingGloves.SetActive(false);
    }
}
